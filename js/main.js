Matter.use('matter-attractors');//Code runs without this
//All errors can be ignored except maybe the Missing use strict statement, the rest happen because of matter and are acceptable
//I created this color scheme and I think it looks pretty nice

// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies;
// create an engine
var engine = Engine.create();

// create a renderer
var render = Render.create({
    element: document.body,
    options: {
        width: 480,
        height: 720,
        wireframes: false,
        background: "#DCE0D9"
    },
    engine: engine

});
var stoppersVisible = false;
var stopperCategory = 0x0001;
var paddleCategory  = 0x0002;
var groundLeft = Bodies.circle(200, (725), 5, {isStatic: true, render: {visible: stoppersVisible}});
var groundRight = Bodies.circle(480 - 200, (725), 5, { isStatic: true, render: {visible: stoppersVisible}});
var ceilLeft = Bodies.circle(200, (-5), 5, {isStatic: true, render: {visible: stoppersVisible}});
var ceilRight = Bodies.circle(480 - 200, (-5), 5, { isStatic: true, render: {visible: stoppersVisible}});
var ball = Bodies.circle(280, 370, 10, {restitution: 0.8, collisionFilter: {group: -2}, friction: 0, render: {fillStyle: "#001a1a"}});
var rightBall = Bodies.circle(1020, 360, 650, {isStatic: true, render: {fillStyle: "#4B3B47"}});
var leftBall = Bodies.circle(-540, 360, 650, {isStatic: true, render: {fillStyle: "#4B3B47"}});
var BottomLeftRect = Bodies.rectangle(85, (720 - 65), 80, 40, {isStatic: true, angle: 0.35, chamfer: {radius: 6}, render: {fillStyle: "#0C120C"}});
var BottomRightRect = Bodies.rectangle(480 - 85, (720 - 65), 80, 40, {isStatic: true, angle: -0.35, chamfer: {radius: 6}, render: {fillStyle: "#0C120C"}});
var TopLeftRect = Bodies.rectangle(85, 65, 80, 40, {isStatic: true, angle: -0.35, chamfer: {radius: 6}, render: {fillStyle: "#0C120C"}});
var TopRightRect = Bodies.rectangle(480 - 85, 65, 80, 40, {isStatic: true, angle : 0.35, chamfer: {radius: 6}, render: {fillStyle: "#0C120C"}});
var topScore = 0;
var BottomScore = 0;
var blStopper = Bodies.rectangle(480 - 130, 720 - 110, 65, 65, {category: stopperCategory, isStatic: true, collisionFilter: {group: -2}, render: {visible: stoppersVisible}});
var brStopper = Bodies.rectangle(130, 720 - 110, 65, 65, {category: stopperCategory, isStatic: true, collisionFilter: {group: -2}, render: {visible: stoppersVisible}});
var trStopper = Bodies.rectangle(480 - 130, 110, 65, 65, {category: stopperCategory, isStatic: true, collisionFilter: {group: -2}, render: {visible: stoppersVisible}});
var tlStopper = Bodies.rectangle(130, 110, 65, 65, {category: stopperCategory, isStatic: true, collisionFilter: {group: -2}, render: {visible: stoppersVisible}});
var peg1 = Bodies.circle(480  / 2, (720 / 2) + 100, 15, {isStatic: true, render: {fillStyle: "#4B3B47"}});
var peg2 = Bodies.circle(480  / 2, (720 / 2) - 100, 15, {isStatic: true, render: {fillStyle: "#4B3B47"}});
// add all of the bodies to the world
World.add(engine.world, [groundLeft, groundRight, ball, rightBall, leftBall, BottomLeftRect, BottomRightRect, TopLeftRect, TopRightRect, ceilLeft, ceilRight, blStopper, brStopper, trStopper, tlStopper, peg1, peg2]);

var bodyr = Bodies.rectangle((480 - 175), 670, 100, 25, {restitution: 0, category: paddleCategory, chamfer: {radius: 7}, render: {fillStyle: "#00004d"}});
var bodyrbrick = Bodies.rectangle(480 - 175, 720 - 40, 100, 50, {render: {visible: stoppersVisible}, density: 0});
var paddlebr = Matter.Body.create({
    parts: [bodyr, bodyrbrick]
});
var constraintBR = Matter.Constraint.create({
    pointA: {x: (480 - 175 + 40), y: 670},
    bodyB: paddlebr,
    pointB: {x: +40, y: 0},
    length: 0
});
engine.world.gravity.y = 0;
World.add(engine.world, [paddlebr, constraintBR]);
var bodyl = Bodies.rectangle(175, 670, 100, 25, {restitution: 0, category: paddleCategory, chamfer: {radius: 7}, render: {fillStyle: "#00004d"}});
var bodylbrick = Bodies.rectangle(175, 720 - 40, 100, 50, {render: {visible: stoppersVisible}, density: 0});
var paddlebl = Matter.Body.create({
    parts: [bodyl, bodylbrick]
});
var constraintBL = Matter.Constraint.create({
    pointA: {x: 175 - 40, y: 670},
    bodyB: paddlebl,
    pointB: {x: -40, y: 0},
    length: 0
});


World.add(engine.world, [paddlebl, constraintBL]);
$('body').on('keydown', function (e) {
    if (e.which === 37) { // left arrow key
        Matter.Body.applyForce(paddlebl, {x: 270, y: 720}, {x: 0, y: -0.085});
    } else if (e.which === 39) { // right arrow key
        Matter.Body.applyForce(paddlebr, {x: 210, y: 720}, {x: 0, y: -0.085});
	}
});
var tbodyr = Bodies.rectangle((480 - 175), 50, 100, 25, {restitution: 0, chamfer: {radius: 7}, render: {fillStyle: "#6B0F1A"}});
var topbodyrbrick = Bodies.rectangle(480 - 175, 40, 100, 50, {render: {visible: stoppersVisible}, density: 0});
var paddletr = Matter.Body.create({
    parts: [tbodyr, topbodyrbrick]
});
var constraintTR = Matter.Constraint.create({
    pointA: {x: (480 - 175 + 40), y: 50},
    bodyB: paddletr,
    pointB: {x: +40, y: 0},
    length: 0
});
World.add(engine.world, [paddletr, constraintTR]);
var tbodyl = Bodies.rectangle(175, 50, 100, 25, {restitution: 0, chamfer: {radius: 7}, render: {fillStyle: "#6B0F1A"}});
var topbodylbrick = Bodies.rectangle(175, 40, 100, 50, {render: {visible: stoppersVisible}, density: 0});
var paddletl = Matter.Body.create({
    parts: [tbodyl, topbodylbrick]
});
var constraintTL = Matter.Constraint.create({
    pointA: {x: 175 - 40, y: 50},
    bodyB: paddletl,
    pointB: {x: -40, y: 0},
    length: 0
});

World.add(engine.world, [paddletl, constraintTL]);
$('body').on('keydown', function (e) {
    if (e.which === 65) { // left arrow key
        Matter.Body.applyForce(paddletl, {x: 270, y: 720}, {x: 0, y: +0.085});
    } else if (e.which === 68) { // right arrow key
        Matter.Body.applyForce(paddletr, {x: 210, y: 720}, {x: 0, y: +0.085});
    }
});
Matter.Events.on(engine, 'beforeUpdate', function () {
	var bodies = Matter.Composite.allBodies(engine.world);
	for (var i = 0; i < bodies.length; i++) {
		var body = bodies[i];
		document.getElementById("topScore").innerHTML = "Top Player's Score: "+topScore;
		document.getElementById("botScore").innerHTML = BottomScore+" : Bottom Player's Score";
		if (body.position.y < 360) {
			body.force.y -= body.mass * 0.001;

		}
		else {
			body.force.y += body.mass * 0.001;    
		}

    if (ball.position.y < -25) {
        BottomScore += 1;
        Matter.Body.setPosition (ball, {x:(100), y:(720 / 2) - 250});
}
    if (ball.position.y > 720+25) {
        topScore += 1;
        Matter.Body.setPosition (ball, {x:(380), y:(720 / 2) + 250});
}
    if (topScore >= 7) {
        alert("Top Player Wins. The game will now reset.")
        topScore = 0
        BottomScore = 0
        Matter.Body.setPosition (ball, {x:(100), y:(720 / 2) - 10});
    }
    if (BottomScore >= 7) {
        alert("Bottom Player Wins. The game will now reset.")
        BottomScore = 0
        topScore = 0
        Matter.Body.setPosition (ball, {x:(100), y:(720 / 2) - 10});
    }
}
});

engine.enableSleeping = true;
// run the engine
Engine.run(engine);
// run the renderer
Render.run(render);
